export interface User {
    id: number;
    email: string;
    password: string;
    token?: String;
    matriculeFiscale:string;
    }
